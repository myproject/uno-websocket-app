const WebSocketServer = require('ws');

const wss = new WebSocketServer.Server({ port: 8081 })
/*
 */
let partys = [];

/*
    ws has new attribue called myAttr
    myAttr containt attribute:
        - score: score of player in party
        - name: name of player
        - party: party where player is in

    messageCode:
     0: unknown problem
     1: not enough arguments
     2: party not found
     3: you are not in a party
     4: name already use in party
     5: you are already in a party
     6: party left
     7: party joined
     8: name not set
     9: player left!
     10: party created
     11: player joined!
     12: you can't change name while in a party
     13: name set
     14: score get succesfully!
     15: player loose!
     16: player score changed!
     17: updated max score.

*/

//new client connected
wss.on("connection", ws => {
    ws.myAttr = {}
    ws.myAttr.score = 0;
    ws.myAttr.name = "";
    ws.myAttr.party = null;

    ws.on("message", data => {
        data = JSON.parse(data);
        executeCommand(data, ws);
    });

    ws.on("close", () => {
        leaveParty(ws);
    });

    ws.onerror = function () {
        console.log("Some Error occurred");
    }
});

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

function executeCommand(args, ws) {
    if(!args["command"]) {
        ws.send(createMessageToJson("error", "fail", "1"));
        return;
    }

    switch(args["command"]) {
        case "create":
            createParty(args, ws);
            break;
        case "join":
            joinParty(args, ws);
            break;
        case "setname":
            setName(args, ws);
            break;
        case "getscore":
            getAllScoresParty(args, ws);
            break;
        case "addscore":
            addScore(args, ws);
            break;
        case "leave":
            leaveParty(ws);
            break;
        case "setscoreparty":
            setScoreMaxParty(args, ws);
            break;
        default:
            ws.send(createMessageToJson("unknown", "fail", "0"));
            break;
    }
}

function setScoreMaxParty(args, ws) {
    if(ws.myAttr.party === null) {
        ws.send(createMessageToJson("setscoreparty", "fail", "3"))
        return;
    }

    ws.myAttr.party.scoreMax = args["score"];
    sendToParty(ws.myAttr.party, createMessageToJson("event", "setscoreparty", "17", [args["score"]]));
}

function leaveParty(ws) {
    if(ws.myAttr.party === null) {
        ws.send(createMessageToJson("leave", "fail", "3"));
        return;
    }

    let party = ws.myAttr.party;
    if(party.players.length === 1) {
        partys.splice(getPartyIndex(party.id), 1);
    } else {
        party.players.splice(getPlayerIndexInParty(party, ws), 1);
    }

    sendToParty(party, createMessageToJson("event", "leave", "9", [ws.myAttr.name]));

    ws.myAttr.party = null;
    ws.send(createMessageToJson("leave", "success", "6"));
}

function createParty(args, ws) {
    if(ws.myAttr.name.trim() === "") {
        ws.send(createMessageToJson("create", "fail", "8"));
        return;
    }

    if(ws.myAttr.party !== null) {
        ws.send(createMessageToJson("create", "fail", "5"));
        return;
    }

    if(!args["scoreMax"]) {
        ws.send(createMessageToJson("create", "fail", "1"));
        return;
    }

    ws.myAttr.score = 0;
    let party = {players: [ws], scoreMax: args["scoreMax"]};
    party.id = s4();
    ws.myAttr.party = party;
    partys.push(party);
    ws.send(createMessageToJson("create", "success", "10", [party.id]));
}

function joinParty(args, ws) {
    if(!args["id"]) {
        ws.send(createMessageToJson("join", "fail", "1"));
        return;
    }

    if(ws.myAttr.party !== null) {
        ws.send(createMessageToJson("join", "fail", "5"));
        return;
    }

    if(ws.myAttr.name.trim() === "") {
        ws.send(createMessageToJson("join", "fail", "8"));
        return;
    }

    let party = getParty(args["id"]);
    if(party === null) {
        ws.send(createMessageToJson("join", "fail", "2"));
        return;
    }

    if(checkIfPlayerNameInParty(party, ws.myAttr.name)) {
        ws.send(createMessageToJson("join", "fail", "4"));
        return;
    }

    sendToParty(party, createMessageToJson("event", "join", "11", [ws.myAttr.name]));
    party.players.push(ws);
    ws.myAttr.party = party;
    ws.myAttr.score = 0;
    ws.send(createMessageToJson("join", "success", "7", [party.scoreMax]));
}

function setName(args, ws) {
    if(!args["name"]) {
        ws.send(createMessageToJson("setname", "fail", "1"));
        return;
    }

    if(ws.myAttr.party !== null) {
        ws.send(createMessageToJson("setname", "fail", "12"));
        return;
    }

    ws.myAttr.name = args["name"].trim();
    ws.send(createMessageToJson("setname", "success", "13", [ws.myAttr.name]));
}

function getAllScoresParty(args, ws) {
    if(ws.myAttr.party === null) {
        ws.send(createMessageToJson("getScore", "fail", "3"));
        return;
    }

    let party = ws.myAttr.party;
    ws.send(createMessageToJson("getScore", "success", "14", [getAllScoresByPartyToJson(party)]));
}

function addScore(args, ws) {
    if(!args["score"]) {
        ws.send(createMessageToJson("addScore", "fail", "1"));
        return;
    }

    if(ws.myAttr.party === null) {
        ws.send(createMessageToJson("addscore", "fail", "3"));
        return;
    }
    ws.myAttr.score += parseInt(args["score"]);

    if(ws.myAttr.score >= 500) {
        sendToParty(ws.myAttr.party, createMessageToJson("event", "endGame", "15", [ws.myAttr.name, ws.myAttr.score]));
    } else {
        sendToParty(ws.myAttr.party, createMessageToJson("event", "score", "16", [ws.myAttr.name, ws.myAttr.score]));
    }
}

function getParty(id) {
    let party = null;

    for(let i = 0; i < partys.length; i++) {
        if(partys[i].id === id) {
            party = partys[i];
        }
    }
    return party;
}

function getPartyIndex(id) {
    let index = -1;

    for(let i = 0; i < partys.length; i++) {
        if(partys[i].id === id) {
            index = i;
        }
    }
    return index;
}

function getPlayerIndexInParty(party, ws) {
    let index = -1;

    for(let i = 0; i < party.players.length; i++) {
        if(party.players[i] === ws) {
            index = i;
        }
    }

    return index;
}

function sendToParty(party, message) {
    for(let i = 0; i < party.players.length; i++) {
        party.players[i].send(message);
    }
}

function checkIfPlayerNameInParty(party, name) {
    let found = false;

    for(let i = 0; i < party.players.length; i++) {
        if(party.players[i].myAttr.name === name) {
            found = true;
        }
    }

    return found;
}

function getAllScoresByPartyToJson(party) {
    let json = '{"score": [';
    for(let i = 0; i < party.players.length; i++) {
        json += '{"name": "' + party.players[i].myAttr.name + '", "score": ' + party.players[i].myAttr.score + "},";
    }
    json = json.substring(0, json.length - 1);
    json += "]}";

    return json;
}

function createMessageToJson(command, state, messageCode, otherArgs = []) {
    let json = '{"command": "' + command + '", "state": "' + state + '", "messageCode": "' + messageCode + '", "args": [';
    for(let i = 0; i < otherArgs.length; i++) {
        if(otherArgs[i].toString().startsWith("{")) {
            json += otherArgs[i] + ",";
        } else {
            json += '"' + otherArgs[i] + '",';
        }
    }

    if(otherArgs.length > 0) {
        json = json.substring(0, json.length - 1);
    }
    json += "]}";

    return json;
}

console.log("The WebSocket server is running on port 8081");